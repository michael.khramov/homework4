﻿using IdentityModel.Client;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace Homework4.ConsoleApp
{
    /// <summary>
    /// Program
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args">Arguments</param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            var httpClientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
            };
            
            var identityServer4Client = new HttpClient(httpClientHandler);

            var discoveryDocument = await identityServer4Client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (discoveryDocument.IsError)
            {
                Console.WriteLine(discoveryDocument.Error);
                return;
            }
            
            // call IS4
            var tokenClient = new TokenClient(identityServer4Client, new TokenClientOptions()
            {
                Address = discoveryDocument.TokenEndpoint,
                ClientId = ConfigurationManager.AppSettings["IdentityServer4.ClientId"],
                ClientSecret = ConfigurationManager.AppSettings["IdentityServer4.ClientSecret"],
            });
            var tokenResponse = await tokenClient.RequestPasswordTokenAsync("bob", "bob", ConfigurationManager.AppSettings["IdentityServer4.ClientScope"]);

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n");

            // call API
            var apiClient = new HttpClient(httpClientHandler);
            apiClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await apiClient.GetAsync("https://localhost:44394/client");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(content);
            }

            Console.ReadLine();
        }
    }
}
