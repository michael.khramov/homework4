﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Homework4.WebAPI.Controllers
{
    /// <summary>
    /// Client Controller
    /// </summary>
    [Route("client")]
    [Authorize]    
    public class ClientController : Controller
    {
        /// <summary>
        /// Get method 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return new JsonResult("Hello World!");
        }
    }
}
